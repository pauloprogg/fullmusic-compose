
# Fullmusic-compose

this is fullmusic-compose repository, it serves as a tray where is the others services each one with own repository. To run this architeture correctly is requirement the docker compose installed in machine.

Through this service will be possible manage the services that make up the entire application. 


## commands to manage service.

To up service that is responsible for every routines related to user run:

  $ docker compose up node-api-users

To up the service that is responsible for mount the register-login-front pages, run:

  $docker compose up register-login-front